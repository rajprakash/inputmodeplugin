var exec = require('cordova/exec');

var InputModePlugin = {
    modeDefault: function () {
        exec(null, null, 'InputModePlugin', 'modeDefault', []);
    },
    modeAjustPan: function () {
        exec(null, null, 'InputModePlugin', 'modeAjustPan', []);
    },
};

module.exports = InputModePlugin;
